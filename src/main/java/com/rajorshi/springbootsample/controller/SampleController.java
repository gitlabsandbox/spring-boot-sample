package com.rajorshi.springbootsample.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

    @RequestMapping(path = "/")
    public String index() {
        return "Hello Gitlab";
    }

}
